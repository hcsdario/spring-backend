/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package former.service;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import former.bean.Form;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import former.persistence.FormsRepository;
import java.util.Optional;
import org.springframework.stereotype.Component;


/**
 *
 * @author LNV_81MVS2IX
 */
@Component

public class FormService /*Impl implements FormService*/  {

    @Autowired
    private FormsRepository repository;   
    
    private ObjectMapper mapper = new ObjectMapper();
    
    public List<Form> getForms() throws JsonProcessingException {
        //List forms = new ArrayList();
        List<Form> formList = null;
        try {
            formList = repository.findAll();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        /*
            for(Form form:formList) {
                // forms.add(mapper.readValue(form,Object.class)); qui da finire
                forms.add(form);
            }
        */
        return formList;

    }
    
    
    public Optional<Form> getForm(String id) /*throws JsonProcessingException*/ {
       
        try {
            return repository.findById(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
        //return mapper.readValue(repository.,Form.class);
    }    
    
    public Form setForm(Form form)throws JsonProcessingException {
        //String config = mapper.writeValueAsString(form);
        return repository.save(form);
    }
    
    // delete torna sempre true perchè deleteById è void
    public boolean deleteForm(String id) /*throws JsonProcessingException*/ {     
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    } 
    
    
}
