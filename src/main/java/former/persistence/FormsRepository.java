/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package former.persistence;

import former.bean.Form;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormsRepository extends MongoRepository<Form, String> {

  // public FormsRepository findByFirstName(String firstName);
  // public List<FormRepository> findByLastName(String lastName);

}