/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package former.controller;

import java.util.List;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import former.bean.Form;
import former.service.FormService;
// import former.persistence.FormsRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;




@RestController
@RequestMapping("/api")
public class FormController {
    
        @Autowired
        private FormService service;
        
	// private static Logger log = LoggerFactory.getLogger(MicrobiologiaController.class);

	@GetMapping("/forms")
	public List<Form> getForms(  ) {
            try {
                return service.getForms();
            } catch (Exception e) {
                // log.debug("MicrobiologiaController::scaricaFileFlusso Eccezione"+e.getMessage());
                System.out.println(e.getMessage());
            }
            return null;                
	}

        @RequestMapping(value = "/form/{id}", method = RequestMethod.GET)
        public Optional<Form> getForm(@PathVariable final String id) {
            return service.getForm(id);
        }
        
        @RequestMapping(value = "/form", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Form setForm( @RequestBody Form form ) throws JsonProcessingException {
            return service.setForm(form);
	}
        
	@RequestMapping(value = "/form/{id}", method = RequestMethod.DELETE)
	public boolean deleteForm( @PathVariable final String id ) {
            return service.deleteForm(id);               
	}
        
        /*
	@GetMapping("/test1")
	public List<Form> test1(  ) throws JsonProcessingException {
            return service.getForms();
	}
        */
        
}
